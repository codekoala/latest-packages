#!/usr/bin/env python

"""
Find the latest versions of Arch packages in the directories of your choosing.
You may also invert the results to show packages that have newer versions in
one of the specified directories.

Author: Josh VanderLinden
Version: 0.1.0
License: BSD
"""

from collections import defaultdict
import argparse
import os
import re
import sys

ARCH = (
    'any',
    'i686',
    'x86_64',
    'armv6h'
)
P = re.compile(r'^(?P<name>.*?)-(?P<ver>(?:(?:\d+\.?){1,}).*?)-(?P<arch>%s)\.pkg\.tar\.xz' % (
    '|'.join(ARCH),
))
V = re.compile(r'[-\.]')

def find_packages(opts):
    versions = defaultdict(dict)
    paths = opts.path

    # make sure we're dealing with a list
    if not isinstance(paths, list):
        paths = [paths]

    for root in paths:
        for f in os.listdir(root):
            m = P.match(f)
            if not m:
                continue

            # turn the version into something we can use to sort/compare
            bits = tuple(int(b) for b in V.split(m.group('ver'))
                         if b.isdigit())

            # keep track of the package path
            f = os.path.join(root, f)
            if not opts.relative:
                f = os.path.abspath(f)

            versions[m.group('name')][bits] = f

    # find the latest versions of each package
    results = {versions[p][max(v.keys())] for p, v in versions.items()}

    if opts.invert:
        # only show the out-of-date packages
        all_packages = {versions[p][v] for p, vs in versions.items() for v in vs}
        results = all_packages - results

    return sorted(results)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Find the latest versions of '
        'packages in the specified directories')
    parser.add_argument('-r', '--relative', action='store_true',
        help='Return relative paths instead of absolute')
    parser.add_argument('-i', '--invert', action='store_true',
        help='Invert the results and show all EXCEPT the latest packages')
    parser.add_argument('path', nargs='*', default='.',
        help='The paths you in which you would like to search for latest packages')


    opts = parser.parse_args()

    sys.stdout.write('\n'.join(find_packages(opts)))
